<div align = center>

# ServerPost

</div>

<p>
Serverless Framework template for TypeScript support.
</p>

## Prerequisites
- Serverless Framework
- Node.js

## Usage

<p>
To create Aws Api Lambda By TypeScript project

```bash
serverpost <projectname>
```

then
```
cd <projectname>
```

install package
```
yarn Or npm i
```

then deploy
```
serverless deploy
```
</p>

## Licence
MIT
